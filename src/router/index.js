import { createRouter, createWebHashHistory } from 'vue-router'
import Layout from '../layout/index.vue'

export const constantRoutes = [
    {
        path: '/',
        component: Layout,
        redirect: '/dashboard',
        children: [
            {
                path: 'dashboard',
                component: () => import('../views/dashboard/index.vue'),
                name: 'Dashboard',
                meta: { title: 'Dashboard', icon: 'dashboard', affix: true }
            }
        ]
    },
    // {
    //     path: '/b/:id/:user',
    //     name: 'B',
    //     component: () => import('../components/b.vue')
    // },
    // {
    //     path: '/user',
    //     name: 'User',
    //     alias: ['/user1', '/user2'],
    //     redirect: (to) => {
    //         console.log(to);
    //         return {
    //             path: '/user/man',
    //             query: to.query
    //         }
    //     },
    //     component: () => import('../components/b.vue'),
    //     children: [{
    //         path: 'man',
    //         name: 'Man',
    //         component: () => import('../components/man.vue')
    //     }, {
    //         path: 'woman',
    //         name: 'WoMan',
    //         component: () => import('../components/woman.vue')
    //     }]
    // },
]

const router = createRouter({
    history: createWebHashHistory(),
    routes: constantRoutes
})



// 重置路由为静态路由
export const resetRouter = () => {
    router.getRoutes().forEach(route => {
        const { name } = route
        if (name && !constantRoutes.find(item => item.name === name)) {
            router.removeRoute(name)
        }
    })
}


export default router