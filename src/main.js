import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'


import svgIcon from "./components/SvgIcon/index.vue";

import 'virtual:svg-icons-register'


createApp(App)
    .use(ElementPlus)
    .use(router)
    .component('svg-icon', svgIcon)
    .mount('#app')
